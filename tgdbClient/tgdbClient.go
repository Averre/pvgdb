// Package containing functions to communicate with thegamesdb.net API.
// Only the data specified by the struct Game in the gameUtil package will be used.
package tgdbClient

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"pvgdb/gameUtil"
)

var baseUrl string = "http://thegamesdb.net/api/"
var getGameByIdService string = "GetGame.php?id="
var getGameByNameService string = "GetGamesList.php?name="

func GetGameByTitle(title string) []gameUtil.Game {
	connectionString := baseUrl + getGameByNameService + title
	result := executeRequest(connectionString)
	return gameUtil.UnmarshalGameXML(result)
}

func GetGameByTitleAndPlatform(title, platform string) string {
	connectionString := baseUrl + getGameByNameService + title + "&platform=" + platform
	return executeRequest(connectionString)
}

func GetGameById(gameId uint16) (gameUtil.Game, error) {
	connectionString := fmt.Sprintf("%s%s%v", baseUrl, getGameByIdService, gameId)
	result := executeRequest(connectionString)
	games := gameUtil.UnmarshalGameXML(result)

	if len(games) == 0 {
		var g gameUtil.Game
		return g, fmt.Errorf("Search for game details with id %v from thegamesdb.net, returned no result.", gameId)
	}

	if len(games) > 1 {
		log.Printf("Search for game details with id %v from thegamesdb.net, returned more than one result. Received %v results", gameId, len(games))
	}

	return games[0], nil
}

func executeRequest(url string) string {
	connection := new(http.Client)
	resp, error := connection.Get(url)

	if error != nil {
		log.Println(error)
	}

	result, error := ioutil.ReadAll(resp.Body)

	if error != nil {
		log.Println(error)
	}

	resp.Body.Close()

	return string(result)
}
