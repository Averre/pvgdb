// Main package containing functions to communicate with thegamesdb.net, and
// to manage the users personal video game database.
// Starts a server on localhost and a main session connected to a local mongodb
// database.
//
// Data retrieved from thegamesdb.net is in XML format, but since we utilize JSON
// when communicating with a pvgdb client, the XML is unmarshalled to a metadata
// struct, and then marshalled to JSON which is either returned to the client or
// persisted.
package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"pvgdb/collectionStorage"
	"pvgdb/collectionStorage/model"
	"pvgdb/gameUtil"
	"pvgdb/tgdbClient"
	"strconv"

	"goji.io"
	"goji.io/pat"
	mgo "gopkg.in/mgo.v2"
)

func searchGamesByTitle(writer http.ResponseWriter, request *http.Request) {
	title := pat.Param(request, "title")

	log.Printf("Searching for %s from thegamesdb.net", title)

	games := tgdbClient.GetGameByTitle(title)
	gamesJSON := gameUtil.MarshalGamesJSON(games)

	messageWithJSON(writer, gamesJSON, http.StatusOK)
}

func getGameDetails(writer http.ResponseWriter, request *http.Request) {
	id := pat.Param(request, "id")

	idAsUint, err := strconv.ParseUint(id, 10, 16)

	if err != nil {
		errorWithJSON(writer, "Could not convert id value to an integer.", http.StatusBadRequest)
		return
	}

	log.Printf("Getting details for game with id %v from thegamesdb.net", id)

	game, _ := tgdbClient.GetGameById(uint16(idAsUint))
	gamesJSON := gameUtil.MarshalGameJSON(game)

	messageWithJSON(writer, gamesJSON, http.StatusOK)
}

func addCollectionGame(session *mgo.Session) func(writer http.ResponseWriter, request *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		var collectionGame model.CollectionGame

		decoder := json.NewDecoder(request.Body)
		err := decoder.Decode(&collectionGame)
		if err != nil {
			errorWithJSON(writer, err.Error(), http.StatusBadRequest)
			return
		}

		// Validate required fields
		err = model.ValidateRequiredFields(collectionGame)
		if err != nil {
			errorWithJSON(writer, err.Error(), http.StatusBadRequest)
			return
		}

		err = addBaseMetadataToCollectionGame(&collectionGame)

		if err != nil {
			errorWithJSON(writer, err.Error(), http.StatusNotFound)
			return
		}

		statusCode, err := collectionStorage.AddCollectionGame(session, collectionGame)

		if err != nil {
			errorWithJSON(writer, err.Error(), statusCode)
			return
		}

		writer.Header().Set("Content-Type", "application/json")
		writer.Header().Set("Location", fmt.Sprintf("%s/%v", request.URL.Path, collectionGame.CollectionId))
		writer.WriteHeader(statusCode)
	}
}

// Fetches the games base metadata from thegamesdb.net and injects it in the
// CollectionGame instance.
func addBaseMetadataToCollectionGame(collectionGame *model.CollectionGame) error {
	log.Printf("Getting details for game with id %v from thegamesdb.net", collectionGame.Game.Id)
	baseGame, err := tgdbClient.GetGameById(uint16(collectionGame.Game.Id))
	collectionGame.Game = baseGame

	return err
}

func updateCollectionGame(session *mgo.Session) func(writer http.ResponseWriter, request *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		id := pat.Param(request, "id")
		var collectionGame model.CollectionGame

		decoder := json.NewDecoder(request.Body)
		err := decoder.Decode(&collectionGame)
		if err != nil {
			errorWithJSON(writer, err.Error(), http.StatusBadRequest)
			return
		}

		// Validate required fields
		err = model.ValidateRequiredFields(collectionGame)
		if err != nil {
			errorWithJSON(writer, err.Error(), http.StatusBadRequest)
			return
		}

		statusCode, err := collectionStorage.UpdateCollectionGame(session, id, collectionGame)

		if err != nil {
			errorWithJSON(writer, err.Error(), statusCode)
			return
		}

		writer.WriteHeader(statusCode)
	}
}

func deleteCollectionGame(session *mgo.Session) func(writer http.ResponseWriter, request *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		id := pat.Param(request, "id")

		statusCode, err := collectionStorage.DeleteCollectionGame(session, id)

		if err != nil {
			errorWithJSON(writer, err.Error(), statusCode)
			return
		}

		writer.WriteHeader(statusCode)
	}
}

func getAllCollectionGames(session *mgo.Session) func(writer http.ResponseWriter, request *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		statusCode, err, result := collectionStorage.AllCollectionGames(session)

		if err != nil {
			errorWithJSON(writer, err.Error(), statusCode)
			return
		}

		messageWithJSON(writer, string(result), statusCode)
	}
}

func getCollectionGameById(session *mgo.Session) func(writer http.ResponseWriter, request *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		id := pat.Param(request, "id")

		statusCode, err, result := collectionStorage.CollectionGameById(id, session)

		if err != nil {
			errorWithJSON(writer, err.Error(), statusCode)
			return
		}

		messageWithJSON(writer, string(result), statusCode)
	}
}

func getCollectionGamesByTitle(session *mgo.Session) func(writer http.ResponseWriter, request *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		title := pat.Param(request, "title")
		title, _ = url.QueryUnescape(title) // Convert %20 and '+' to whitespace

		statusCode, err, result := collectionStorage.CollectionGamesByTitle(title, session)

		if err != nil {
			errorWithJSON(writer, err.Error(), statusCode)
			return
		}

		messageWithJSON(writer, string(result), statusCode)
	}
}

func getCollectionGamesByPlatform(session *mgo.Session) func(writer http.ResponseWriter, request *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		platform := pat.Param(request, "platform")
		platform, _ = url.QueryUnescape(platform) // Convert %20 and '+' to whitespace

		statusCode, err, result := collectionStorage.CollectionGamesByPlatform(platform, session)

		if err != nil {
			errorWithJSON(writer, err.Error(), statusCode)
			return
		}

		messageWithJSON(writer, string(result), statusCode)
	}
}

func getAllAvailablePlatforms(session *mgo.Session) func(writer http.ResponseWriter, request *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		statusCode, err, result := collectionStorage.AllAvailablePlatforms(session)

		if err != nil {
			errorWithJSON(writer, err.Error(), statusCode)
			return
		}

		messageWithJSON(writer, string(result), statusCode)
	}
}

func messageWithJSON(w http.ResponseWriter, message string, code int) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(code)
	fmt.Fprint(w, message)
}

func errorWithJSON(w http.ResponseWriter, message string, code int) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(code)
	fmt.Fprintf(w, "{message: %q}", message)
}

// Maps handlers to the available service URLs, and starts the server.
func main() {
	session := collectionStorage.NewStorageSession()
	defer session.Close() // Ensure that the main mongodb session closes at shutdown of the server.

	mux := goji.NewMux()

	// Handlers for searching thegamesdb.net
	mux.HandleFunc(pat.Get("/"), http.NotFound)
	mux.HandleFunc(pat.Get("/games/:title"), searchGamesByTitle)
	mux.HandleFunc(pat.Get("/details/:id"), getGameDetails)

	// Handlers for managing collection database

	// GET requests
	mux.HandleFunc(pat.Get("/collectionGames"), getAllCollectionGames(session))
	mux.HandleFunc(pat.Get("/collectionGames/:id"), getCollectionGameById(session))
	mux.HandleFunc(pat.Get("/collectionGames/title/:title"), getCollectionGamesByTitle(session))
	mux.HandleFunc(pat.Get("/collectionGames/platform/:platform"), getCollectionGamesByPlatform(session))
	mux.HandleFunc(pat.Get("/platforms"), getAllAvailablePlatforms(session))

	// POST requests
	mux.HandleFunc(pat.Post("/collectionGames"), addCollectionGame(session))

	// PUT requests
	mux.HandleFunc(pat.Put("/collectionGames/:id"), updateCollectionGame(session))

	// DELETE requests
	mux.HandleFunc(pat.Delete("/collectionGames/:id"), deleteCollectionGame(session))

	http.ListenAndServe("localhost:8080", mux)
}
