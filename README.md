# pvgdb (Personal Video Game Database) #

This project allows the user to keep track of his/her personal video game collection, and utilize metadata from thegamesdb.net.

### Functionality ###

The project includes a server with services that passes on the request to thegamesdb.net, converts the resulting XML
to a struct, and in the end re-convert it to JSON.

### Prerequisites ###

* A local mongodb instance.
* go 1.9

### Running ###

Start the mongodb server with:
```
mongod --dbpath /{path_to_mongodb_location}
```

Start the server with:
```
go run pvgdbServer.go
```

Search games from thegamesdb.net with e.g:
```
curl http://localhost:8080/games/Sonic -H "Content-Type: application/json" -X GET
```

Get game details from thegamesdb.net with e.g:
```
curl http://localhost:8080/details/3014 -H "Content-Type: application/json" -X GET
```

Get collection games
All:
```
curl http://localhost:8080/collectionGames -H "Content-Type: application/json" -X GET
```
By id, e.g:
```
curl http://localhost:8080/collectionGames/5aaec6258efcd0b990619891 -H "Content-Type: application/json" -X GET
```
By title, e.g:
```
curl http://localhost:8080/collectionGames/title/Sonic -H "Content-Type: application/json" -X GET
```
By platform, e.g:
```
curl http://localhost:8080/collectionGames/platform/Sega+Genesis -H "Content-Type: application/json" -X GET
```

Get all available platforms:
```
curl http://localhost:8080/platforms -H "Content-Type: application/json" -X GET
```

Add a new collection game, e.g:
```
curl http://localhost:8080/collectionGames -H "Content-Type: application/json" -d @{json file} -X POST
```

Update a collection game, e.g:
```
curl http://localhost:8080/collectionGames/5aaec6258efcd0b990619891 -H "Content-Type: application/json" -d @{json file} -X PUT
```

Delete a collection game, e.g:
```
curl http://localhost:8080/collectionGames/5aaec6258efcd0b990619891 -H "Content-Type: application/json" -X DELETE
```
