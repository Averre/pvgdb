// Package with functions to handle the personal video games database,
// persisted in an instance of a local mongodb database.
package collectionStorage

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"pvgdb/collectionStorage/model"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// Set to true if you which to clear the database on startup of the server.
//
// TODO: Read the value from an external configuration file.
var isDrop = false

// Creates an instance of the main session from which all other client sessions
// will be copied from.
func NewStorageSession() *mgo.Session {
	session, err := mgo.Dial("localhost")
	if err != nil {
		panic(err)
	}

	session.SetMode(mgo.Monotonic, true)
	ensureIndex(session)

	return session
}

// Connects to the database and sets up the unique index of the CollectionGame
// documents.
func ensureIndex(s *mgo.Session) {
	session := s.Copy()
	defer session.Close()

	if isDrop {
		err := session.DB("store").DropDatabase()

		if err != nil {
			panic(err)
		}
	}

	c := session.DB("store").C("collectionGames")

	index := mgo.Index{
		Key:        []string{"CollectionId"},
		Unique:     true,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}

	err := c.EnsureIndex(index)
	if err != nil {
		panic(err)
	}
}

func AddCollectionGame(s *mgo.Session, collectionGame model.CollectionGame) (int, error) {
	session := s.Copy()
	defer session.Close()

	c := session.DB("store").C("collectionGames")

	err := c.Insert(collectionGame)
	if err != nil {
		if mgo.IsDup(err) {
			log.Fatal("Tried to save a collection game with a duplicate Id.", err)
			return http.StatusBadRequest, errors.New("Collection game with this CollectionId already exists")
		}

		log.Fatal("Failed insert collection game: ", err)
		return http.StatusInternalServerError, errors.New("Database error")
	}

	return http.StatusCreated, nil
}

func UpdateCollectionGame(s *mgo.Session, id string, collectionGame model.CollectionGame) (int, error) {
	session := s.Copy()
	defer session.Close()

	c := session.DB("store").C("collectionGames")

	err := c.UpdateId(bson.ObjectIdHex(id), &collectionGame)
	if err != nil {
		switch err {
		default:
			log.Fatal("Failed update of collection game: ", err)
			return http.StatusInternalServerError, errors.New("Database error")
		case mgo.ErrNotFound:
			return http.StatusNotFound, errors.New("Collection game not found")
		}
	}

	return http.StatusNoContent, nil
}

func DeleteCollectionGame(s *mgo.Session, id string) (int, error) {
	session := s.Copy()
	defer session.Close()

	c := session.DB("store").C("collectionGames")
	err := c.RemoveId(bson.ObjectIdHex(id))

	if err != nil {
		switch err {
		default:
			log.Fatal("Failed delete of collection game: ", err)
			return http.StatusInternalServerError, errors.New("Database error")
		case mgo.ErrNotFound:
			return http.StatusNotFound, errors.New("Collection game not found")
		}
	}

	return http.StatusNoContent, nil
}

func AllCollectionGames(s *mgo.Session) (int, error, []byte) {
	session := s.Copy()
	defer session.Close()

	c := session.DB("store").C("collectionGames")

	var collectionGames []model.CollectionGame
	err := c.Find(bson.M{}).All(&collectionGames)
	if err != nil {
		log.Fatal("Failed get all collection games: ", err)
		return http.StatusInternalServerError, errors.New("Database error"), []byte("")
	}

	respBody, err := json.MarshalIndent(collectionGames, "", "  ")
	if err != nil {
		log.Fatal(err)
		return http.StatusInternalServerError, errors.New("Server error"), nil
	}

	return http.StatusOK, nil, respBody
}

func CollectionGameById(id string, s *mgo.Session) (int, error, []byte) {
	session := s.Copy()
	defer session.Close()

	c := session.DB("store").C("collectionGames")

	var collectionGame model.CollectionGame
	err := c.FindId(bson.ObjectIdHex(id)).One(&collectionGame)
	if err != nil {
		log.Fatal("Failed get collection game with id %s: ", id)
		return http.StatusInternalServerError, errors.New("Database error"), []byte("")
	}

	respBody, err := json.MarshalIndent(collectionGame, "", "  ")
	if err != nil {
		log.Fatal(err)
		return http.StatusInternalServerError, errors.New("Server error"), nil
	}

	return http.StatusOK, nil, respBody
}

func CollectionGamesByTitle(title string, s *mgo.Session) (int, error, []byte) {
	fmt.Printf("Searching for games with the title %s.", title)

	session := s.Copy()
	defer session.Close()

	c := session.DB("store").C("collectionGames")

	var collectionGames []model.CollectionGame
	err := c.Find(bson.M{"game.gametitle": bson.RegEx{".*" + title + ".*", "i"}}).All(&collectionGames)
	if err != nil {
		log.Fatal("Failed get collection games by title: ", err)
		return http.StatusInternalServerError, errors.New("Database error"), []byte("")
	}

	respBody, err := json.MarshalIndent(collectionGames, "", "  ")
	if err != nil {
		log.Fatal(err)
		return http.StatusInternalServerError, errors.New("Server error"), nil
	}

	return http.StatusOK, nil, respBody
}

func CollectionGamesByPlatform(platform string, s *mgo.Session) (int, error, []byte) {
	fmt.Printf("Searching for games for the platform %s.", platform)

	session := s.Copy()
	defer session.Close()

	c := session.DB("store").C("collectionGames")

	var collectionGames []model.CollectionGame
	err := c.Find(bson.M{"game.platform.platform": platform}).All(&collectionGames)
	if err != nil {
		log.Fatal("Failed get collection games by platform: ", err)
		return http.StatusInternalServerError, errors.New("Database error"), []byte("")
	}

	respBody, err := json.MarshalIndent(collectionGames, "", "  ")
	if err != nil {
		log.Fatal(err)
		return http.StatusInternalServerError, errors.New("Server error"), nil
	}

	return http.StatusOK, nil, respBody
}

func AllAvailablePlatforms(s *mgo.Session) (int, error, []byte) {
	fmt.Printf("Retrieving all available platforms.")

	session := s.Copy()
	defer session.Close()

	c := session.DB("store").C("collectionGames")

	var platforms []string
	err := c.Find(nil).Distinct("game.platform.platform", &platforms)
	if err != nil {
		log.Fatal("Failed get platforms: ", err)
		return http.StatusInternalServerError, errors.New("Database error"), []byte("")
	}

	respBody, err := json.MarshalIndent(platforms, "", "  ")
	if err != nil {
		log.Fatal(err)
		return http.StatusInternalServerError, errors.New("Server error"), nil
	}

	return http.StatusOK, nil, respBody
}
