package model

import (
	"encoding/json"
	"fmt"
)

type Rating int

const (
	ZERO Rating = iota
	ONE
	TWO
	THREE
	FOUR
	FIVE
)

func RatingValue(r int) (Rating, error) {
	switch r {
	case 0:
		return ZERO, nil
	case 1:
		return ONE, nil
	case 2:
		return TWO, nil
	case 3:
		return THREE, nil
	case 4:
		return FOUR, nil
	case 5:
		return FIVE, nil
	}

	return 0, fmt.Errorf("%d does not belong to Rating values", r)
}

func (r Rating) MarshalJSON() ([]byte, error) {
	return json.Marshal(int(r))
}

func (r *Rating) UnmarshalJSON(data []byte) error {
	var ratingValue int

	if err := json.Unmarshal(data, &ratingValue); err != nil {
		return fmt.Errorf("Mediatype should be an integer, got %s", data)
	}

	var err error
	*r, err = RatingValue(ratingValue)
	return err

}
