// Package containing the model for collection games.
// The main struct is CollectionGame, which includes the metadata from
// thegamesdb.net as a nested document.
package model

import (
	"fmt"
	"pvgdb/gameUtil"

	"gopkg.in/mgo.v2/bson"
)

type CollectionGame struct {
	CollectionId bson.ObjectId `bson:"_id,omitempty"`

	// Pointers to distinguish between missing and zero values
	State     *State
	Region    *Region
	Mediatype *Mediatype

	Media  Rating
	Case   Rating
	Manual Rating

	Comment string

	Game gameUtil.Game
}

func ValidateRequiredFields(collectionGame CollectionGame) error {
	if collectionGame.State == nil {
		return fmt.Errorf("Missing value for required field State.")
	} else if collectionGame.Region == nil {
		return fmt.Errorf("Missing value for required field Region.")
	} else if collectionGame.Mediatype == nil {
		return fmt.Errorf("Missing value for required field Mediatype.")
	} else if collectionGame.Game.Id <= 0 {
		return fmt.Errorf("Missing value for required field Game.Id.")
	}

	return nil
}
