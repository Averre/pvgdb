# PVGDBClient (Personal Video Game Database Client) #

This subproject allows the user to keep track of his/her personal video game collection, by communicating with the pvgdbserver.

### Functionality ###

The project includes an ios/android app written using react native.
Through the app the user can manage his/her personal video games collection, with metadata provided by thegamesdb.net.

### Prerequisites ###

* Node.js
* React Native Comman Line Interface
