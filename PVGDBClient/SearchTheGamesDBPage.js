'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Button,
  ActivityIndicator,
  Image,
} from 'react-native';
import TheGamesDBResults from './TheGamesDBResults';

function urlForQueryAndPage(key, value, pageNumber) {
  const data = {};
  data[key] = value;

  const querystring = Object.keys(data)
    .map(key => key + '=' + encodeURIComponent(data[key]))
    .join('&');

  return 'https://api.nestoria.co.uk/api?' + querystring;
}

export default class SearchTheGamesDBPage extends Component<{}> {
  constructor(props) {
    super(props);
    this.state = {
      searchString: '',
      isLoading: false,
      message: '',
    };
  }

  _onSearchTextChanged = (event) => {
    this.setState({ searchString: event.nativeEvent.text });
  };

  _onSearchPressed = () => {
    const service = 'http://localhost:8080/games/';
    const query = service + encodeURIComponent(this.state.searchString);
    this._executeQuery(query);
  };

  _executeQuery = (query) => {
    console.log(query);
    this.setState({ isLoading: true });

    fetch(query)
      .then(response => response.json())
      .then(json => this._handleResponse(json))
      .catch(error =>
         this.setState({
          isLoading: false,
          message: 'Something bad happened ' + error
       }));
  };

  _handleResponse = (response) => {
    this.setState({ isLoading: false , message: '' });

    if (response != null) {
      this.props.navigator.push({
        title: 'Results',
        component: TheGamesDBResults,
        passProps: {games: response}
      });
    } else {
      this.setState({ message: 'Game not found. Please try again.'});
    }
  };

  render() {
    const spinner = this.state.isLoading ? <ActivityIndicator size='large'/> : null;

    return (
      <View style={styles.container}>
        <View style={styles.flowRight}>
          <TextInput
            style={styles.searchInput}
            value={this.state.searchString}
            onChange={this._onSearchTextChanged}
            placeholder='Search via title'/>
          <Button
            onPress={this._onSearchPressed}
            color='#48BBEC'
            title='Go'/>
        </View>
        {spinner}
        <Text style={styles.description}>{this.state.message}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  description: {
    marginBottom: 20,
    fontSize: 18,
    textAlign: 'center',
    color: '#656565'
  },
  container: {
    padding: 30,
    marginTop: 65,
    alignItems: 'center'
  },
  flowRight: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  searchInput: {
    height: 36,
    padding: 4,
    marginRight: 5,
    flexGrow: 1,
    fontSize: 18,
    borderWidth: 1,
    borderColor: '#48BBEC',
    borderRadius: 8,
    color: '#48BBEC',
  },
});
