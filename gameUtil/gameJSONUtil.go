// Functions to marshal game metadata structures retrieved from thegamesdb to JSON.
// Used when returning search data to a pvgdb client.
package gameUtil

import (
	"encoding/json"
	"log"
)

func MarshalGameJSON(game Game) string {
	games := []Game{game}
	return MarshalGamesJSON(games)
}

func MarshalGamesJSON(games []Game) string {
	gamesAsJSON, err := json.Marshal(games)

	if err != nil {
		log.Printf("Marshaling struct to JSON failed: %v", err)
		return ""
	}

	return string(gamesAsJSON)
}
