// Package with game metadata structure, and utility functions.
package gameUtil

import (
	"encoding/xml"
	"log"
)

type Images struct {
	BaseImgUrl  string
	Fanart      []string `xml:"Images>fanart>original"`
	Boxart      []string `xml:"Images>boxart"`
	Screenshots []string `xml:"Images>screenshot>original"`
	ClearLogo   string   `xml:"clearLogo"`
}

type Platform struct {
	PlatformId uint16
	Platform   string
}

type Game struct {
	Id          uint16 `xml:"id" json: ",omitempty"`
	GameTitle   string `json: ",omitempty"`
	ReleaseDate string
	Genres      []string `xml:"Genres>genre"`
	Youtube     string
	Platform
	Images
}

type Result struct {
	XMLName    xml.Name `xml:"Data"`
	BaseImgUrl string   `xml:"baseImgUrl"`
	Games      []Game   `xml:"Game"`
}

func UnmarshalGameXML(xmlData string) []Game {
	gamesResult := Result{}

	err := xml.Unmarshal([]byte(xmlData), &gamesResult)
	if err != nil {
		log.Printf("Unmarshaling of xmlData failed: %v", err)
		return nil
	}

	// Hack to insert the value from baseImgUrl to every game.
	for index, game := range gamesResult.Games {
		game.BaseImgUrl = gamesResult.BaseImgUrl
		gamesResult.Games[index] = game
	}

	return gamesResult.Games
}
