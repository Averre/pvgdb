// Unmarshalling tests.
package gameUtil

import (
	"fmt"
	"testing"
)

var idTestValue uint16 = 27257
var gameTitleTestValue string = "Sonic Crackers"
var releaseDateTestValue string = "04/20/2009"
var platformTestValue string = "Sega Genesis"

/* Details test variables */
var baseImgUrlTestValue string = "http://thegamesdb.net/banners/"

var testXml string = fmt.Sprintf(`
  <Data>
  <Game>
  <id>%v</id>
  <GameTitle>%s</GameTitle>
  <ReleaseDate>%s</ReleaseDate>
  <Platform>%s</Platform>
  </Game>
  </Data>`,
	idTestValue,
	gameTitleTestValue,
	releaseDateTestValue,
	platformTestValue)

var gameDetailsTestXml string = fmt.Sprintf(`
<Data>
<baseImgUrl>%s</baseImgUrl>
<Game>
<id>2684</id>
<GameTitle>Alex Kidd in Miracle World</GameTitle>
<PlatformId>35</PlatformId>
<Platform>Sega Master System</Platform>
<ReleaseDate>07/01/1986</ReleaseDate>
<Overview>Journey through the planet Aries to the beautiful City of Radactian - and save it from the evil Janken the Great.</Overview>
<Images>
<fanart>
<original width="1920" height="1080">fanart/original/2684-1.jpg</original>
<thumb>fanart/thumb/2684-1.jpg</thumb>
</fanart>
<fanart>
<original width="1920" height="1080">fanart/original/2684-2.jpg</original>
<thumb>fanart/thumb/2684-2.jpg</thumb>
</fanart>
<fanart>
<original width="1920" height="1080">fanart/original/2684-3.jpg</original>
<thumb>fanart/thumb/2684-3.jpg</thumb>
</fanart>
<fanart>
<original width="1920" height="1080">fanart/original/2684-4.jpg</original>
<thumb>fanart/thumb/2684-4.jpg</thumb>
</fanart>
<fanart>
<original width="1280" height="720">fanart/original/2684-5.jpg</original>
<thumb>fanart/thumb/2684-5.jpg</thumb>
</fanart>
<boxart side="back" width="1563" height="2100" thumb="boxart/thumb/original/back/2684-1.jpg">boxart/original/back/2684-1.jpg</boxart>
<boxart side="front" width="1554" height="2100" thumb="boxart/thumb/original/front/2684-1.jpg">boxart/original/front/2684-1.jpg</boxart>
<screenshot>
<original width="1024" height="768">screenshots/2684-1.jpg</original>
<thumb>screenshots/thumb/2684-1.jpg</thumb>
</screenshot>
<screenshot>
<original width="259" height="194">screenshots/2684-2.jpg</original>
<thumb>screenshots/thumb/2684-2.jpg</thumb>
</screenshot>
<clearlogo width="400" height="136">clearlogo/2684.png</clearlogo>
</Images>
</Game>
</Data>
`,
	baseImgUrlTestValue)

func TestUnmarshalGameXMLFromSearch(t *testing.T) {
	games := UnmarshalGameXML(testXml)

	if len(games) != 1 {
		t.Errorf("Incorrect amount of games unmarshalled. Expected 1. Array contained %v", len(games))
	}

	if games[0].Id != idTestValue {
		t.Errorf("Incorrect Id. Expected %v, received %v", idTestValue, games[0].Id)
	}

	if games[0].GameTitle != gameTitleTestValue {
		t.Errorf("Incorrect GameTitle. Expected %s, received %s", gameTitleTestValue, games[0].GameTitle)
	}

	if games[0].ReleaseDate != releaseDateTestValue {
		t.Errorf("Incorrect ReleaseDate. Expected %s, received %s", releaseDateTestValue, games[0].ReleaseDate)
	}

	if games[0].Platform.Platform != platformTestValue {
		t.Errorf("Incorrect Platform. Expected %s, received %s", platformTestValue, games[0].Platform.Platform)
	}
}

func TestUnmarshalGameDetailsXML(t *testing.T) {
	games := UnmarshalGameXML(gameDetailsTestXml)

	if len(games) != 1 {
		t.Errorf("Incorrect amount of games unmarshalled. Expected 1. Array contained %v", len(games))
	}

	game := games[0]

	if game.BaseImgUrl != baseImgUrlTestValue {
		t.Errorf("Incorrect BaseImgUrl. Expected %s, received %s", baseImgUrlTestValue, game.BaseImgUrl)
	}

	// TODO: Test the image arrays.
}
